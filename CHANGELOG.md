# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

### Changed

- Now the library uses `SipHasher` from an external crate instead of
  the deprecated standard-library hasher.
- Output style for `hamming_answer` so it is similar to `hamming_check`'s output.
- `ToSequenceIndex::to_index` now returns `Option<usize>`.
- `BitPosition::new` now returns `Option<BitPosition>` when the `number` argument is 0.
- Now `BitPosition` fields `kind` and `number` are private.

## [1.0.0-beta.2] — 2016-10-16

### Added

- Non-interactive mode to `hamming`, in which no questions are asked
  interactively.
- Method `Generator::generate_task_suite`.
- Field `number` to `Task`.

### Changed

- Default answers file name now contains current date and time.
- `Generator::generate_task` now borrows `self` immutably.
- All executables now generate equivalent task suites with the
  `Generator::generate_task_suite` method.

## [1.0.0-beta.1] — 2016-10-16

### Added

- Point earning for each task.
- Option `--percentage-only` in `hamming_check` for printing out
  resulting percentage without the whole report.
- Method `Task::complexity()`
- Documentation for all units.

### Changed

- The `Verdict` scale. Now this enum defines the degree of answer's
  correctness, including partial correctness.
- Now the `hamming_check` executable fails if there is more than one
  argument.

## [1.0.0-beta] — 2016-10-08

### Added

- The `hamming_check` executable to check answers stored in a file.
- If the answers file exists, you can choose the name of file in which
  your answers will be placed.

### Changed

- The `hamming` executable now saves given answers to a text file.
- Method `Task::check_answer` is renamed to `Task::check` and returns
  a `Verdict` value.
- Method `Generator::new` now borrows a `Student` instance instead of
  taking it.

## [0.2.1] — 2016-10-03

### Fixed

- Double task output in the `hamming_answers` executable.

## [0.2.0] — 2016-10-03

### Added

- Method `Task::correct_answer`.
- Method `Sequence::syndrome`.
- An executable for printing out correct answers.

### Changed

- Rename `Task::check` to `Task::check_answer`.
- Move executables into `src/bin`.

## [0.1.1] — 2016-10-03

### Fixed

- Incorrect Hamming code generation with respect to the last bit 
  of a sequence.

## 0.1.0 — 2016-10-02

### Added

- Hamming code generation and correction.
- Command-line utility for interacting with students.
- Printing tasks and asking questions.
- Printing out a bit table along with a linear representation of a sequence.
- Collecting and checking answers.
- Help message and usage examples.

[Unreleased]: https://gitlab.com/edubenetskiy/hamming/compare/v1.0.0-beta.2...HEAD
[1.0.0-beta.2]: https://gitlab.com/edubenetskiy/hamming/compare/v1.0.0-beta.1...v1.0.0-beta.2
[1.0.0-beta.1]: https://gitlab.com/edubenetskiy/hamming/compare/v1.0.0-beta...v1.0.0-beta.1
[1.0.0-beta]: https://gitlab.com/edubenetskiy/hamming/compare/v0.2.1...v1.0.0-beta
[0.2.1]: https://gitlab.com/edubenetskiy/hamming/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/edubenetskiy/hamming/compare/v0.1.1...v0.2.0
[0.1.1]: https://gitlab.com/edubenetskiy/hamming/compare/v0.1.0...v0.1.1
