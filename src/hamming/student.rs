/// A struct representing a student.
#[derive(Hash)]
pub struct Student {
    surname: String,
    given_names: String,
    patronymic: String,
    group: String,
}

impl Student {
    pub fn new<S>(surname: S, given_names: S, patronymic: S, group: S) -> Self
        where S: ToString
    {
        Student {
            surname: surname.to_string(),
            given_names: given_names.to_string(),
            patronymic: patronymic.to_string(),
            group: group.to_string(),
        }
    }

    pub fn surname(&self) -> &str {
        self.surname.as_str()
    }
    pub fn given_names(&self) -> &str {
        self.given_names.as_str()
    }
    pub fn patronymic(&self) -> &str {
        self.patronymic.as_str()
    }

    pub fn group(&self) -> &str {
        self.group.as_str()
    }
}
