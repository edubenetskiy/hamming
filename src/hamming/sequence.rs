use std::ops::{Index, IndexMut};
use std::fmt::{self, Display};
use std::string::ToString;
use std::str::FromStr;
use bit::Bit;
use bit_position::BitPosition;

/// Types implementing this trait may be converted into an index within a `Sequence`.
pub trait ToSequenceIndex {
    /// Return an index corresponding to `self`. The index `0` must correspond
    /// to the very first bit of a `Sequence`, that is the indexing is zero-based.
    /// If the conversion fails, the function must return `None`.
    fn to_index(&self) -> Option<usize>;
}

// This is an implementation of converting a `usize` Hamming syndrome into an index.
// Syndrome 0 does not correspond to any bit in a `Sequence`, because the first bit
// is encoded with syndrome 1.
impl ToSequenceIndex for usize {
    fn to_index(&self) -> Option<usize> {
        match *self {
            0 => None,
            _ => Some(self - 1),
        }
    }
}

/// A Hamming-encoded sequence of `Bit`s.
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Sequence {
    inner: Vec<Bit>,
}

impl Sequence {
    pub fn new(vector: Vec<Bit>) -> Self {
        Sequence { inner: vector }
    }

    /// Return the length of the sequence.
    pub fn len(&self) -> usize {
        self.inner.len()
    }

    /// Turn a sequence into a valid Hamming code. May be useful
    /// when it is needed to encode a randomly generated sequence.
    pub fn encode(&self) -> Self {
        let mut data = self.clone();

        for i in 0.. {
            let parity_bit: usize = 2usize.pow(i);
            if parity_bit >= data.len() {
                break;
            }

            data[parity_bit] = Bit::Zero;
            for j in parity_bit..(data.len() + 1) {
                if j & parity_bit > 0 {
                    data[parity_bit] ^= data[j];
                }
            }
        }
        data
    }

    /// Returns a corrected sequence.
    pub fn fix(&self) -> Self {
        let mut data = self.clone();

        if let Some(mistake_position) = data.mistake_position() {
            data[mistake_position] = !data[mistake_position];
        }

        data
    }

    /// Return whether the sequence is a correct Hamming code.
    pub fn is_correct(&self) -> bool {
        self.syndrome() == 0
    }

    /// Return a `BitPosition` denoting the position of a mistake made in the Hamming code.
    pub fn mistake_position(&self) -> Option<BitPosition> {
        if self.is_correct() {
            None
        } else {
            Some(BitPosition::from(self.syndrome().to_index().expect("unreachable")))
        }
    }

    fn syndrome(&self) -> usize {
        let mut position = 0;
        for i in 0.. {
            let parity_bit = 2usize.pow(i);
            if parity_bit > self.len() {
                break;
            };

            let mut local_syndrome = 0;
            for j in 1..(self.len() + 1) {
                let value: usize = self[j].into();
                if j & parity_bit > 0 {
                    local_syndrome += value;
                }
            }
            position += parity_bit * (local_syndrome % 2);
        }

        position
    }

    /// Return a `String` containing the tabular representation of the sequence.
    ///
    /// Example:
    ///
    /// ```rust
    /// use hamming::sequence::Sequence;
    /// use std::str::FromStr;
    ///
    /// let sequence = Sequence::from_str("0010111").unwrap();
    ///
    /// let table = "
    /// +---+---+---+---+---+---+---+
    /// | r1| r2| i1| r3| i2| i3| i4|
    /// +---+---+---+---+---+---+---+
    /// |  0|  0|  1|  0|  1|  1|  1|
    /// +---+---+---+---+---+---+---+
    /// ";
    ///
    /// assert_eq!(table.trim(), sequence.as_table());
    /// ```
    pub fn as_table(&self) -> String {
        let length = self.len();
        let positions = (0..length).map(BitPosition::from);

        let mut splitter = String::new();
        let mut header = String::new();
        let mut data = String::new();

        splitter.push('+');
        header.push('|');
        data.push('|');

        for position in positions {
            splitter.push_str("---+");
            header.push_str(format!("{:>3}|", position.to_string()).as_str());
            data.push_str(format!("{:>3}|", self[position].to_string()).as_str());
        }

        format!("{}\n{}\n{}\n{}\n{}",
                splitter,
                header,
                splitter,
                data,
                splitter)
    }
}

impl<T> Index<T> for Sequence
    where T: ToSequenceIndex
{
    type Output = Bit;
    fn index(&self, n: T) -> &Self::Output {
        &self.inner[n.to_index().unwrap()]
    }
}

impl<T> IndexMut<T> for Sequence
    where T: ToSequenceIndex
{
    fn index_mut(&mut self, n: T) -> &mut Bit {
        &mut self.inner[n.to_index().unwrap()]
    }
}

impl Display for Sequence {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for bit in &self.inner {
            try!(write!(f, "{}", bit.to_string()));
        }
        Ok(())
    }
}

/// An enum containing possible errors that may be raised after transforming a `str`
/// into a `Sequence`.
#[derive(Debug)]
pub enum SequenceParseError {
    EmptySequence,
    NonBinaryCharactersFound,
    OversizedSequence,
}

impl FromStr for Sequence {
    type Err = SequenceParseError;

    fn from_str(arg: &str) -> Result<Sequence, Self::Err> {
        match arg.len() {
            0 => Err(SequenceParseError::EmptySequence),
            x if x < 32 => {
                let mut vector: Vec<Bit> = vec![];
                for char in arg.chars() {
                    match char {
                        '0' => vector.push(Bit::Zero),
                        '1' => vector.push(Bit::One),
                        _ => return Err(SequenceParseError::NonBinaryCharactersFound),
                    }
                }
                Ok(Sequence::new(vector))
            }
            _ => return Err(SequenceParseError::OversizedSequence),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;

    #[test]
    fn sequence_indexing_with_usize() {
        assert_eq!(None, 0usize.to_index());
        assert_eq!(Some(0), 1usize.to_index());
        assert_eq!(Some(1), 2usize.to_index());
        assert_eq!(Some(2), 3usize.to_index());
        assert_eq!(Some(3), 4usize.to_index());
        assert_eq!(Some(4), 5usize.to_index());
        assert_eq!(Some(5), 6usize.to_index());
        assert_eq!(Some(6), 7usize.to_index());
        assert_eq!(Some(7), 8usize.to_index());
    }

    #[test]
    fn sequence_encoding() {
        assert!(Sequence::from_str("0101010").ok().unwrap().encode().is_correct());
        assert!(Sequence::from_str("0111100").ok().unwrap().encode().is_correct());
        assert!(Sequence::from_str("1110100").ok().unwrap().encode().is_correct());
        assert!(Sequence::from_str("1000110").ok().unwrap().encode().is_correct());

        assert!(Sequence::from_str("100000000000000").ok().unwrap().encode().is_correct());
        assert!(Sequence::from_str("101011000000000").ok().unwrap().encode().is_correct());
        assert!(Sequence::from_str("111101010110110").ok().unwrap().encode().is_correct());
        assert!(Sequence::from_str("000001010001111").ok().unwrap().encode().is_correct());
        assert!(Sequence::from_str("000000000000001").ok().unwrap().encode().is_correct());
    }

    #[test]
    fn sequence_fixing() {
        let sequence = Sequence::from_str("0010100").ok().unwrap();
        let corrected_sequence = Sequence::from_str("0010110").ok().unwrap();
        assert_eq!(sequence.fix(), corrected_sequence);

        let sequence = Sequence::from_str("011111010011101").ok().unwrap();
        let corrected_sequence = Sequence::from_str("011111010001101").ok().unwrap();
        assert_eq!(sequence.fix(), corrected_sequence);
    }
}
