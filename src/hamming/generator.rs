use std::hash::{Hash, Hasher};
use rand::{Rng, SeedableRng, StdRng};
use siphasher::sip::SipHasher;

use task::Task;
use sequence::Sequence;
use bit::Bit;
use student::Student;

/// An enum containing values of how often a generator should produce
/// mistakes in Hamming codes.
///
/// Using the `WithRate` arm you prescribe the generator to produce
/// `errors` wrong sequences per `correct` right sequences. For example,
/// `MakeError::WithRate { errors: 1, correct: 4 }` will generate wrong
/// sequences with the 20% probability.
pub enum MakeError {
    Never,
    WithRate { errors: usize, correct: usize },
    Always,
}

/// A task generator.
pub struct Generator {
    hash: usize,
}

impl Generator {
    /// Create an instance of `Generator` seeded by the data known about a student,
    /// the number of a variant, and the value of the secret salt defined in `lib.rs`.
    pub fn new(student: &Student, year: u32, variant_number: u32) -> Generator {
        let mut hasher = SipHasher::new();
        student.hash(&mut hasher);
        variant_number.hash(&mut hasher);
        year.hash(&mut hasher);
        super::SECRET_SALT.hash(&mut hasher);

        let computed_hash = hasher.finish() as usize;
        Generator { hash: computed_hash }
    }

    /// Generate a suite of five tasks. 4 tasks are 7 bits long, 5th task is 15 bits long.
    /// Tasks 1..4 always contain bit errors, task 5 may or may not contain an error
    /// (producing 4 sequences with an error per 1 sequence without any errors).
    pub fn generate_task_suite(&self) -> Vec<Task> {
        let mut generated_tasks = vec![];

        for task_number in 1..6 {
            let size: usize;
            let with_error: MakeError;

            if task_number < 5 {
                size = 7;
                with_error = MakeError::Always;
            } else {
                size = 15;
                with_error = MakeError::WithRate {
                    errors: 4,
                    correct: 1,
                };
            }

            let task = self.generate_task(task_number, size, with_error);
            generated_tasks.push(task);
        }

        generated_tasks
    }

    /// Generate a new `Task` instance with or without errors and return it.
    pub fn generate_task(&self, task_number: usize, size: usize, with_error: MakeError) -> Task {
        let mut hasher = SipHasher::new();
        self.hash.hash(&mut hasher);
        task_number.hash(&mut hasher);

        let mut generator = StdRng::from_seed(&[hasher.finish() as usize]);

        let mut sequence: Vec<Bit> = vec![];
        for _ in 0..size {
            sequence.push(generator.gen());
        }

        let mut sequence = Sequence::new(sequence).encode();

        let should_have_a_mistake = match with_error {
            MakeError::Never => false,
            MakeError::WithRate { errors, correct } => {
                let total_cases = errors + correct;
                generator.gen::<usize>() % total_cases < errors
            }
            MakeError::Always => true,
        };

        if should_have_a_mistake {
            let position = (generator.gen::<usize>() % size) + 1;
            sequence[position] = !sequence[position];
        }

        Task::new(task_number, sequence)
    }
}
