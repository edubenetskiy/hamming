use std::str::FromStr;
use std::fmt::{self, Display};

/// An enum specifying the kind of a bit within a sequence. Each bit
/// in the Hamming sequence may be either informational or auxiliary
/// (so-called parity bit).
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum BitKind {
    Informational,
    Parity,
}

impl FromStr for BitKind {
    type Err = ();

    fn from_str(string: &str) -> Result<Self, ()> {
        if string.len() != 1 {
            return Err(());
        } else {
            match string.chars().next().unwrap() {
                'i' | 'I' => Ok(BitKind::Informational),
                'r' | 'R' => Ok(BitKind::Parity),
                _ => Err(()),
            }
        }
    }
}

impl Display for BitKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f,
                    "{}",
                    match *self {
                        BitKind::Parity => "r",
                        BitKind::Informational => "i",
                    }));
        Ok(())
    }
}
