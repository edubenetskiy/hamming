//! A library for generating and processing [Hamming code] sequences,
//! as well as producing exercises in Hamming decoding and checking
//! students’ solutions.
//!
//! [Hamming code]: https://en.wikipedia.org/wiki/Hamming_code

extern crate rand;
extern crate siphasher;

pub mod bit;
pub mod bit_kind;
pub mod bit_position;
pub mod sequence;
pub mod generator;
pub mod task;
pub mod student;

/// Return the version number of the library.
pub fn version() -> String {
    env!("CARGO_PKG_VERSION").to_string()
}

// One can change this secret salt in order to prevent
// users from previewing the tasks.
static SECRET_SALT: i32 = 1948;
