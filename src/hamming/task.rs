use sequence::{Sequence, ToSequenceIndex};
use bit_position::BitPosition;
use std::str::FromStr;

pub struct Task {
    number: usize,
    sequence: Sequence,
}

/// A struct representing a student's answer to a task.
#[derive(Debug, Eq, PartialEq)]
pub enum Answer {
    ReceivedCorrectly,
    ReceivedWithMistake {
        position: BitPosition,
        corrected_sequence: Sequence,
    },
}

/// The place of the mistake made in a partially correct answer.
#[derive(Eq, PartialEq)]
pub enum PlaceOfMistake {
    WrongPosition,
    WrongSequence,
}

/// The reason why an answer is incorrect.
#[derive(Eq, PartialEq)]
pub enum IncorrectnessReason {
    SequenceContainsAError,
    SequenceIsFreeOfErrors,
    PositionOutOfBounds,
    WrongSequenceAndPosition,
}

use self::IncorrectnessReason::*;

/// The degree of correctness of an `Answer` with respect to the task.
#[derive(Eq, PartialEq)]
pub enum Verdict {
    Correct,
    PartiallyCorrect(PlaceOfMistake),
    Incorrect(IncorrectnessReason),
}

impl Verdict {
    pub fn is_correct(&self) -> bool {
        *self == Verdict::Correct
    }

    pub fn is_partially_correct(&self) -> bool {
        match *self {
            Verdict::PartiallyCorrect(_) => true,
            _ => false,
        }
    }

    pub fn is_incorrect(&self) -> bool {
        match *self {
            Verdict::Incorrect(_) => true,
            _ => false,
        }
    }

    #[inline]
    pub fn max_points() -> usize {
        10
    }

    pub fn points(&self) -> usize {
        match *self {
            Verdict::Correct => 10,
            Verdict::PartiallyCorrect(PlaceOfMistake::WrongPosition) => 5,
            Verdict::PartiallyCorrect(PlaceOfMistake::WrongSequence) => 7,
            Verdict::Incorrect(_) => 0,
        }
    }
}

/// An enum containing possible errors that may be raised after transforming a set
/// of comma-separated values into an `Answer`.
#[derive(Debug)]
pub enum AnswerParseError {
    InvalidState,
    InvalidPosition,
    InvalidSequence,
    PositionNotFound,
    SequenceNotFound,
}

impl Answer {
    /// Encode an answer into CSV.
    pub fn as_csv(&self) -> String {
        match *self {
            Answer::ReceivedCorrectly => format!("ReceivedCorrectly"),
            Answer::ReceivedWithMistake { ref position, ref corrected_sequence } => {
                format!("ReceivedWithMistake,{},{}", position, corrected_sequence)
            }
        }
    }

    /// Read an `Answer` from CSV.
    pub fn from_csv(str: &str) -> Result<Self, AnswerParseError> {
        let mut iter = str.split(',');
        match iter.next() {
            Some("ReceivedCorrectly") => Ok(Answer::ReceivedCorrectly),
            Some("ReceivedWithMistake") => {
                let position: BitPosition;
                let sequence: Sequence;

                if let Some(pos) = iter.next() {
                    if let Ok(pos) = BitPosition::from_str(pos) {
                        position = pos
                    } else {
                        return Err(AnswerParseError::InvalidPosition);
                    }
                } else {
                    return Err(AnswerParseError::PositionNotFound);
                };

                if let Some(seq) = iter.next() {
                    if let Ok(seq) = Sequence::from_str(seq) {
                        sequence = seq
                    } else {
                        return Err(AnswerParseError::InvalidSequence);
                    }
                } else {
                    return Err(AnswerParseError::SequenceNotFound);
                }

                Ok(Answer::ReceivedWithMistake {
                    position: position,
                    corrected_sequence: sequence,
                })
            }
            _ => Err(AnswerParseError::InvalidState),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use sequence::Sequence;
    use bit_position::BitPosition;
    use std::str::FromStr;

    #[test]
    fn answer_to_csv() {
        assert_eq!(Answer::ReceivedCorrectly.as_csv(), "ReceivedCorrectly");

        assert_eq!(Answer::ReceivedWithMistake {
                           position: BitPosition::from_str("i1").unwrap(),
                           corrected_sequence: Sequence::from_str("0100101").unwrap(),
                       }
                       .as_csv(),
                   "ReceivedWithMistake,i1,0100101");
    }

    #[test]
    fn answer_from_csv() {
        assert_eq!(Answer::from_csv("ReceivedCorrectly").ok(),
                   Some(Answer::ReceivedCorrectly));

        assert_eq!(Answer::from_csv("ReceivedWithMistake,i3,1110001").ok(),
                   Some(Answer::ReceivedWithMistake {
                       position: BitPosition::from_str("i3").unwrap(),
                       corrected_sequence: Sequence::from_str("1110001").unwrap(),
                   }));
    }
}

/// A task containing a Hamming code sequence.
impl Task {
    pub fn new(number: usize, sequence: Sequence) -> Self {
        Task {
            number: number,
            sequence: sequence,
        }
    }

    /// Return the task number.
    pub fn number(&self) -> usize {
        self.number
    }

    /// Return a sequence which should be decoded by a student.
    pub fn sequence<'a>(&'a self) -> &'a Sequence {
        &self.sequence
    }

    /// Return the correct answer to the task.
    pub fn correct_answer(&self) -> Answer {
        match self.sequence.mistake_position() {
            Some(position) => {
                Answer::ReceivedWithMistake {
                    position: position,
                    corrected_sequence: self.sequence.fix(),
                }
            }
            None => Answer::ReceivedCorrectly,
        }
    }

    pub fn complexity(&self) -> usize {
        self.sequence.len()
    }

    /// Check a given answer.
    pub fn check(&self, answer: &Answer) -> Verdict {
        let sequence = self.sequence();

        match *answer {
            Answer::ReceivedCorrectly => {
                if sequence.is_correct() {
                    Verdict::Correct
                } else {
                    Verdict::Incorrect(SequenceContainsAError)
                }
            }
            Answer::ReceivedWithMistake { ref position, ref corrected_sequence } => {
                if sequence.is_correct() {
                    return Verdict::Incorrect(SequenceIsFreeOfErrors);
                } else if position.to_index().expect("unreachable") >= sequence.len() {
                    return Verdict::Incorrect(PositionOutOfBounds);
                } else {
                    let position_is_correct = *position == sequence.mistake_position().unwrap();
                    let sequence_is_correct = *corrected_sequence == sequence.fix();

                    match (position_is_correct, sequence_is_correct) {
                        (true, true) => Verdict::Correct,
                        (true, false) => Verdict::PartiallyCorrect(PlaceOfMistake::WrongSequence),
                        (false, true) => Verdict::PartiallyCorrect(PlaceOfMistake::WrongPosition),
                        (false, false) => Verdict::Incorrect(WrongSequenceAndPosition),
                    }
                }
            }
        }
    }
}
