use std::str::FromStr;
use std::fmt::{self, Display};
use sequence::ToSequenceIndex;
use bit_kind::BitKind;

/// A position of a bit within a Hamming code, indicating its `BitKind`
/// and index number in a `Sequence`.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct BitPosition {
    kind: BitKind,
    number: usize,
}

impl BitPosition {
    /// Create a new `BitPosition`. There can not be zeroth informational
    /// or parity bit, and the method returns `None` if `number` is 0.
    pub fn new(kind: BitKind, number: usize) -> Option<Self> {
        match number {
            0 => None,
            _ => {
                Some(BitPosition {
                    kind: kind,
                    number: number,
                })
            }
        }
    }
}

impl Display for BitPosition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "{}{}", self.kind.to_string(), self.number));
        Ok(())
    }
}

impl ToSequenceIndex for BitPosition {
    fn to_index(&self) -> Option<usize> {
        match self.kind {
            BitKind::Informational => {
                let mut offset: usize = 0;
                loop {
                    offset += 1;
                    if 2usize.pow(offset as u32) - offset as usize > self.number {
                        break;
                    }
                }
                Some(offset + self.number - 1)
            }
            BitKind::Parity => Some(2usize.pow(self.number as u32 - 1) - 1),
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum BitPositionParseError {
    WrongBitType,
    WrongNumberFormat,
    ZeroPosition,
    EmptyString,
    NumberTooBig,
}

impl FromStr for BitPosition {
    type Err = BitPositionParseError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        if string.len() == 0 {
            return Err(BitPositionParseError::EmptyString);
        }

        if let Ok(kind) = string[0..1].parse() {
            if let Ok(number) = string[1..].parse() {
                match number {
                    0 => Err(BitPositionParseError::ZeroPosition),
                    x if x < 32 => Ok(BitPosition::new(kind, number).expect("unreachable")),
                    _ => Err(BitPositionParseError::NumberTooBig),
                }
            } else {
                Err(BitPositionParseError::WrongNumberFormat)
            }
        } else {
            Err(BitPositionParseError::WrongBitType)
        }
    }
}

impl From<usize> for BitPosition {
    fn from(number: usize) -> Self {
        let mut informational_index = 0;
        let mut parity_index = 0;
        let mut kind = BitKind::Parity;

        for index in 0..(number + 1) {
            let position = index + 1;
            let is_power_of_two = position & (position - 1) == 0;
            if is_power_of_two {
                parity_index += 1;
                kind = BitKind::Parity;
            } else {
                informational_index += 1;
                kind = BitKind::Informational;
            }
        }

        let index = match kind {
            BitKind::Parity => parity_index,
            BitKind::Informational => informational_index,
        };

        BitPosition::new(kind, index).expect("unreachable")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;
    use sequence::ToSequenceIndex;
    use bit_kind::BitKind::{self, Informational, Parity};

    fn bit_position(kind: BitKind, number: usize) -> BitPosition {
        assert!(number > 0);
        BitPosition::new(kind, number).expect("unreachable")
    }

    #[test]
    fn invalid_bit_positions() {
        assert_eq!(None, BitPosition::new(Parity, 0));
        assert_eq!(None, BitPosition::new(Informational, 0));
    }

    #[test]
    fn parse_bit_position_from_string() {
        assert_eq!("r1".parse(), Ok(bit_position(Parity, 1)));
        assert_eq!("R25".parse(), Ok(bit_position(Parity, 25)));
        assert_eq!("i5".parse(), Ok(bit_position(Informational, 5)));
        assert_eq!("I31".parse(), Ok(bit_position(Informational, 31)));

        assert_eq!(BitPosition::from_str("i36"),
                   Err(BitPositionParseError::NumberTooBig));
        assert_eq!(BitPosition::from_str("r0"),
                   Err(BitPositionParseError::ZeroPosition));
    }

    #[test]
    fn bit_position_from_index() {
        assert_eq!(bit_position(Parity, 1), BitPosition::from(0));
        assert_eq!(bit_position(Parity, 2), BitPosition::from(1));
        assert_eq!(bit_position(Informational, 1), BitPosition::from(2));
        assert_eq!(bit_position(Parity, 3), BitPosition::from(3));
        assert_eq!(bit_position(Informational, 2), BitPosition::from(4));
        assert_eq!(bit_position(Informational, 3), BitPosition::from(5));
        assert_eq!(bit_position(Informational, 4), BitPosition::from(6));
        assert_eq!(bit_position(Parity, 4), BitPosition::from(7));
    }

    #[test]
    fn sequence_indexing_with_bit_position() {
        assert_eq!(Some(0), bit_position(Parity, 1).to_index());
        assert_eq!(Some(1), bit_position(Parity, 2).to_index());
        assert_eq!(Some(2), bit_position(Informational, 1).to_index());
        assert_eq!(Some(3), bit_position(Parity, 3).to_index());
        assert_eq!(Some(4), bit_position(Informational, 2).to_index());
        assert_eq!(Some(5), bit_position(Informational, 3).to_index());
        assert_eq!(Some(6), bit_position(Informational, 4).to_index());
        assert_eq!(Some(7), bit_position(Parity, 4).to_index());
    }
}
