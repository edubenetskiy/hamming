use std::ops::{BitXor, BitXorAssign, Not};
use std::fmt::{self, Display};
use rand::{Rand, Rng};

/// A binary digit.
#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub enum Bit {
    Zero,
    One,
}

use self::Bit::{Zero, One};

impl Rand for Bit {
    fn rand<R: Rng>(rng: &mut R) -> Self {
        match rng.gen::<bool>() {
            false => Zero,
            true => One,
        }
    }
}

impl Into<usize> for Bit {
    fn into(self) -> usize {
        match self {
            Zero => 0usize,
            One => 1usize,
        }
    }
}

impl BitXor<Bit> for Bit {
    type Output = Bit;
    fn bitxor(self, rhs: Bit) -> Self::Output {
        match (self, rhs) {
            (Zero, Zero) => Zero,
            (Zero, One) => One,
            (One, Zero) => One,
            (One, One) => Zero,
        }
    }
}

impl BitXorAssign<Bit> for Bit {
    fn bitxor_assign(&mut self, rhs: Bit) {
        *self = self.bitxor(rhs);
    }
}

impl Not for Bit {
    type Output = Bit;
    fn not(self) -> Self::Output {
        match self {
            Zero => One,
            One => Zero,
        }
    }
}

impl Display for Bit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Bit::Zero => write!(f, "0"),
            Bit::One => write!(f, "1"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Bit::{Zero, One};

    #[test]
    fn bit_xor() {
        assert_eq!(Zero ^ Zero, Zero);
        assert_eq!(Zero ^ One, One);
        assert_eq!(One ^ Zero, One);
        assert_eq!(One ^ One, Zero);
    }

    #[test]
    fn bit_inversion() {
        assert_eq!(!One, Zero);
        assert_eq!(!Zero, One);
    }

    #[test]
    fn bit_displaying() {
        assert_eq!(Zero.to_string(), "0");
        assert_eq!(One.to_string(), "1");
    }
}
