extern crate hamming;
extern crate regex;
extern crate time;
extern crate getopts;

use hamming::sequence::{Sequence, SequenceParseError};
use hamming::bit_position::{BitPosition, BitPositionParseError};
use hamming::task::{Task, Answer};
use hamming::student::Student;
use hamming::generator::Generator;

use getopts::{Options, Fail};
use regex::Regex;

use std::env;
use std::io::{self, Write};
use std::fs::{OpenOptions, File};
use std::path::Path;

pub const COPYING: &'static str = "
Copyright © 2016 Егор Дубенецкий
Лицензия MIT: https://opensource.org/licenses/MIT
Это свободное ПО: вы можете изменять и распространять его.
Нет НИКАКИХ ГАРАНТИЙ до степени, разрешённой законом.
";

fn interrogate_student(task: &Task) -> Answer {
    print_task(task);
    let there_is_a_mistake = read_bool("Имеются ли в сообщении ошибки?");
    let answer = if there_is_a_mistake {
        let position = read_bit_position("В каком бите допущена ошибка?");
        let sequence = read_sequence("Запишите исправленное сообщение.");
        Answer::ReceivedWithMistake {
            position: position,
            corrected_sequence: sequence,
        }
    } else {
        Answer::ReceivedCorrectly
    };
    println!("Ответ принят.");
    answer
}

fn print_task(task: &Task) {
    println!("Принята двоичная последовательность: {}.",
             task.sequence());
    println!("{}", task.sequence().as_table());
}

fn read_bool<S>(question: S) -> bool
    where S: Into<String>
{
    let question = question.into();
    loop {
        let mut buf = String::new();
        print!("{} [да/+/нет/-] ", question);
        io::stdout().flush().ok();
        io::stdin().read_line(&mut buf).ok();
        let input = buf.trim().to_lowercase();
        let input = match input.as_ref() {
            "y" | "yes" | "д" | "да" | "+" => Some(true),
            "n" | "no" | "н" | "нет" | "-" => Some(false),
            _ => None,
        };
        if let Some(value) = input {
            return value;
        }
    }
}

fn read_sequence<S>(question: S) -> Sequence
    where S: Into<String>
{
    let question = question.into();
    loop {
        let mut buf = String::new();
        print!("{} [последовательность нулей и единиц] ",
               question);
        io::stdout().flush().ok();
        io::stdin().read_line(&mut buf).ok();
        let input = buf.trim();

        let result = input.parse();

        match result {
            Ok(sequence) => return sequence,
            Err(err) => {
                match err {
                    SequenceParseError::EmptySequence => {
                        println!("Последовательность битов не может быть пустой.");
                    }
                    SequenceParseError::OversizedSequence => {
                        println!("Размер сообщения должен быть меньше 32 битов.");
                    }
                    SequenceParseError::NonBinaryCharactersFound => {
                        println!("Сообщение не может содержать знаки, отличные от ‘0’ и ‘1’.");
                    }
                }
            }
        }
    }
}

fn read_filename<S>(question: S) -> String
    where S: Into<String>
{
    let question = question.into();
    loop {
        let mut buf = String::new();
        print!("{} [введите имя файла] ", question);
        io::stdout().flush().ok();
        io::stdin().read_line(&mut buf).ok();
        let input = buf.trim();
        if !input.is_empty() {
            return input.to_string();
        }
    }
}

fn read_bit_position<S>(question: S) -> BitPosition
    where S: Into<String>
{
    let question = question.into();
    loop {
        let mut buf = String::new();
        print!("{} [укажите тип и номер бита, например ‘r4’] ",
               question);
        io::stdout().flush().ok();
        io::stdin().read_line(&mut buf).ok();
        let input = buf.trim();

        let result = input.parse();

        match result {
            Ok(sequence) => return sequence,
            Err(err) => {
                match err {
                    BitPositionParseError::EmptyString => {
                        println!("Пожалуйста, опишите позицию в формате ‘tNNN’, где ‘t’ —");
                        println!("это одна из букв ‘i’ или ‘r’, соответствующая типу бита,");
                        println!("а NNN — ненулевой десятичный номер бита.");
                    }
                    BitPositionParseError::NumberTooBig => {
                        println!("Слишком большое число. Введите номер, меньший 32.")
                    }
                    BitPositionParseError::WrongBitType => {
                        println!("Неверный тип бита. Допускается ‘i’ или ‘r’.")
                    }
                    BitPositionParseError::WrongNumberFormat => {
                        println!("После типа бита должен следовать десятичный номер бита.")
                    }
                    BitPositionParseError::ZeroPosition => {
                        println!("Номер бита не может быть равен нулю: это не индекс массива.")
                    }
                }
            }
        }
    }
}

fn print_usage(program_name: &str) {
    println!("Использование: {} [опции] ФАМИЛИЯ ИМЯ ОТЧЕСТВО ГРУППА ГОД ВАРИАНТ",
             program_name);
    println!("Генерирует и выводит набор заданий по синтезу и анализу кода Хэмминга.");
    println!();
    println!("Опции:");
    println!("    -I, --non-interactive    выводить только задания, ничего не спрашивать");
    println!();
    println!("ФАМИЛИЯ, ИМЯ и ОТЧЕСТВО должны быть написаны кириллицей в соответствии");
    println!("с данными ИСУ ИТМО в любом регистре. Если ОТЧЕСТВО отсутствует, необходимо");
    println!("поставить дефисоминус “-”, обычно имеющийся на клавиатуре.");
    println!("Если имя или фамилия состоит из нескольких слов, разделённых пробелами,");
    println!("не забудьте заключить их в одинарные или двойные кавычки.");
    println!();
    println!("Номер ГРУППЫ должен быть записан в виде буквенно-цифрового кода, например");
    println!("“P3110”. Номер ВАРИАНТА должен быть записан арабскими цифрами согласно");
    println!("выданному преподавателем варианту, например “42”. ГОД должен соответствовать");
    println!("текущей дате по григорианскому календарю.");
    println!();
    println!("Примеры:");
    println!("    hamming Абстрактный Павел Васильевич Z2170 2020 42");
    println!("    hamming Клей 'Кассиус Марселлус' - N3201 2020 57");
}

fn print_help(program_name: &str) {
    print_usage(program_name);
    println!();
    println!("Версия {}.", hamming::version());
    println!("{}", COPYING);
}

fn resolve_answers_file_name() -> String {
    let current_time = time::now_utc()
        .strftime("%Y%m%d%H%M%S")
        .map(|x| x.to_string())
        .unwrap_or(format!("unknown-time"));

    let mut filename: String = format!("answers-{}.txt", current_time);

    loop {
        {
            let path = Path::new(&filename);
            if path.exists() {
                println!("Файл «{}», куда должны быть записаны ответы, уже существует.",
                         filename);

                if read_bool(format!("Удалить файл «{}» и создать новый?",
                                     filename)) {
                    if std::fs::remove_file(&path).is_ok() {
                        println!("Файл «{}» удалён.", &filename);
                        println!();
                        break;
                    } else {
                        println!("Не удалось удалить файл «{}».",
                                 &filename);
                        println!("Укажите другое имя файла или удалите файл вручную.");
                    }
                } else {
                    println!("Укажите другое имя файла.");
                }
            } else {
                break;
            }
        }
        filename = read_filename("В какой файл сохранять ваши ответы?");
        println!();
    }

    filename
}

pub fn main() {
    let args: Vec<String> = env::args().collect();
    let program_name = args[0].clone();

    let mut opts: Options = Options::new();
    opts.optflag("I",
                 "non-interactive",
                 "выводить только задания, ничего не спрашивать");

    let matches: getopts::Matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            let error_message = match f {
                Fail::ArgumentMissing(opt) => {
                    format!("Параметр “{}” требует аргумента.",
                            opt)
                }
                Fail::OptionDuplicated(opt) => {
                    format!("Параметр “{}” не может повторяться.",
                            opt)
                }
                Fail::OptionMissing(opt) => {
                    format!("Требуется параметр “{}”.", opt)
                }
                Fail::UnexpectedArgument(opt) => {
                    format!("Параметр “{}” не требует аргумента.",
                            opt)
                }
                Fail::UnrecognizedOption(opt) => {
                    format!("Неизвестный параметр: “{}”.", opt)
                }
            };
            println!("ОШИБКА. {}", error_message);
            print_help(&program_name);
            return;
        }
    };

    let args = &matches.free;
    if args.len() != 6 {
        println!("Ошибка синтаксиса.");
        print_help(&program_name);
        return;
    }

    let name_re: Regex = Regex::new(r"^[- '\w]+$").unwrap();
    let group_re: Regex = Regex::new(r"^[:alnum:]+$").unwrap();

    let surname = args[0].trim().to_uppercase();
    let given_names = args[1].trim().to_uppercase();
    let patronymic = args[2].trim().to_uppercase();

    if !name_re.is_match(surname.as_str()) {
        println!("Фамилия должна быть записана кириллицей.");
        return;
    }

    if !name_re.is_match(given_names.as_str()) {
        println!("Имя должно быть записано кириллицей.");
        return;
    }

    if !name_re.is_match(patronymic.as_str()) {
        println!("Отчество должно быть записано кириллицей.");
        println!("Если отчество отсутствует, укажите “-”.");
        return;
    }

    let group = args[3].trim().to_uppercase();
    if !group_re.is_match(group.as_str()) {
        println!("Номер группы должен быть записан в виде алфавитно-цифрового");
        println!("кода, где все буквы (если имеются) записаны латиницей.");
        return;
    }

    let year = args[4].trim().parse();
    if year.is_err() {
        println!("Год должен быть записан по григорианскому календарю в виде десятичного числа.");
        return;
    }
    let year: u32 = year.unwrap();

    let variant_number: Result<u32, _> = args[5].trim().parse();
    if variant_number.is_err() {
        println!("Номер варианта должен быть целым неотрицательным числом.");
        return;
    }
    let variant_number = variant_number.unwrap();

    println!("------------------------------------------------------------------------");
    println!("Ваше имя: {} {} {}.",
             &surname,
             &given_names,
             &patronymic);
    println!("Вы обучаетесь в группе {}. Вариант № {}, {} год.",
             &group,
             &variant_number,
             &year);
    println!("ВНИМАНИЕ! Проверьте вышеприведённые данные и исправьте параметры");
    println!("командной строки, если это необходимо.");
    if !matches.opt_present("non-interactive") {
        println!("Прервать исполнение программы можно в любой момент с помощью комбинации");
        println!("клавиш Ctrl+C. В таком случае ваши ответы будут потеряны, а программа");
        println!("немедленно завершит работу.");
    }
    println!("------------------------------------------------------------------------");
    println!();

    let student: Student = Student::new(surname, given_names, patronymic, group);
    let generator = Generator::new(&student, year, variant_number);
    let tasks = generator.generate_task_suite();

    if matches.opt_present("non-interactive") {
        // Non-interactive mode. Print tasks only.
        for task in tasks {
            println!("Задание {}", task.number());
            print_task(&task);
            println!("Имеются ли в сообщении ошибки? Если да, то укажите позицию");
            println!("допущенной ошибки (тип и номер бита, например “r4”)");
            println!("и запишите исправленное сообщение.");
            println!();
        }
    } else {
        // Interactive mode. Print tasks and interrogate the user, saving answers to a file.
        let filename = resolve_answers_file_name();
        let opened = OpenOptions::new().create(true).write(true).open(&filename);
        let mut file: File;

        if let Ok(f) = opened {
            file = f;
            // Print the file header.
            let result = writeln!(file,
                                  "{},{},{},{},{},{}",
                                  student.surname(),
                                  student.given_names(),
                                  student.patronymic(),
                                  student.group(),
                                  year,
                                  variant_number);
            if result.is_err() {
                println!("Не удалось записать информацию о студенте в файл.");
                println!("Возможно, для записи недостаточно прав доступа.");
                println!("Попробуйте запустить программу ещё раз и указать другое имя файла.");
                return;
            }
        } else {
            println!("Невозможно открыть (создать) файл «{}» для записи ответов.",
                     filename);
            println!("Попробуйте запустить программу ещё раз и указать другое имя файла.");
            return;
        }

        let mut score: usize = 0;
        let mut max_score: usize = 0;

        for task in tasks {
            println!("Задание {}", task.number());
            let answers = interrogate_student(&task);
            if writeln!(file, "{}", answers.as_csv()).is_err() {
                println!("Не удалось записать ответ в файл.");
                println!("Возможно, для записи недостаточно прав доступа.");
                println!("Попробуйте запустить программу ещё раз и указать другое имя файла.");
                return;
            }

            if task.check(&answers).is_correct() {
                score += 1
            }

            max_score += 1;
            println!();
        }

        let word = if 10 < (score % 100) && (score % 100) < 20 {
            "заданий"
        } else {
            match score % 10 {
                1 => "задание",
                2 | 3 | 4 => "задания",
                _ => "заданий",
            }
        };

        println!("Вы верно решили {} {} из {}.",
                 score,
                 word,
                 max_score);
        println!("Ваши ответы записаны в файл «{}».",
                 filename);
    }
}
