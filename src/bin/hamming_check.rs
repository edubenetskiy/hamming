extern crate hamming;
extern crate getopts;

use hamming::task::{Task, Answer, Verdict, PlaceOfMistake, IncorrectnessReason};
use hamming::student::Student;
use hamming::generator::Generator;

use getopts::{Options, Fail};

use std::env;
use std::io::{BufReader, BufRead};
use std::fs::{OpenOptions, File};

pub const COPYING: &'static str = "
Copyright © 2016 Егор Дубенецкий
Лицензия MIT: https://opensource.org/licenses/MIT
Это свободное ПО: вы можете изменять и распространять его.
Нет НИКАКИХ ГАРАНТИЙ до степени, разрешённой законом.
";

fn print_task(task: &Task) {
    println!("Задание:");
    println!("    Принята двоичная последовательность: {}.",
             task.sequence());
    for line in task.sequence().as_table().lines() {
        println!("    {}", line);
    }
    println!("    Имеются ли в сообщении ошибки?");
}

fn print_answer(answer: &Answer) {
    match *answer {
        Answer::ReceivedCorrectly => println!("    Последовательность принята без ошибок."),
        Answer::ReceivedWithMistake { ref position, ref corrected_sequence } => {
            println!("    В последовательности была допущена ошибка в бите {}.",
                     position);
            println!("    Исправленная последовательность: {}.",
                     corrected_sequence);
            for line in corrected_sequence.as_table().lines() {
                println!("    {}", line);
            }
        }
    }
}

fn print_usage(program_name: &str) {
    println!("Использование: {} [-p] ФАЙЛ", program_name);
    println!("Проверяет файл с ответами к заданиям по синтезу и анализу кода Хэмминга.");
    println!();
    println!("Параметры:");
    println!("    -p, --percentage-only    выводить только процент выполнения работы,");
    println!("                             а не полный отчёт с указанием на ошибки");
}

fn print_help(program_name: &str) {
    print_usage(program_name);
    println!();
    println!("Версия {}.", hamming::version());
    println!("{}", COPYING);
}

fn short_verdict_description(verdict: &Verdict) -> &str {
    match *verdict {
        Verdict::Correct => "решено верно",
        Verdict::PartiallyCorrect(ref pom) => {
            match *pom {
                PlaceOfMistake::WrongSequence => "неверно указана исправленная последовательность",
                PlaceOfMistake::WrongPosition => "неверно указана позиция допущенной ошибки",
            }
        }
        Verdict::Incorrect(ref reason) => {
            match *reason {
                IncorrectnessReason::PositionOutOfBounds => {
                    "указанная позиция лежит вне последовательности"
                }
                IncorrectnessReason::SequenceContainsAError => {
                    "имеющаяся ошибка не была определена"
                }
                IncorrectnessReason::SequenceIsFreeOfErrors => {
                    "студент исправил несуществующую ошибку"
                }
                IncorrectnessReason::WrongSequenceAndPosition => {
                    "неверные позиция и последовательность"
                }
            }
        }
    }
}

fn print_header(student: &Student,
                variant_number: u32,
                year: u32,
                verdicts: &Vec<Verdict>,
                sum: usize,
                max_sum: usize,
                percentage: usize) {
    println!("+------------------------------------------------------------------+");
    println!("| {:^64} |", "СТУДЕНТ");
    println!("+---------+--------------------------------------------------------+");
    println!("| ФИО     | {:<54} |",
             format!("{} {} {}",
                     student.surname(),
                     student.given_names(),
                     student.patronymic()));
    println!("| Группа  | {:<54} |", student.group());
    println!("| Вариант | {:<54} |",
             format!("№ {}, {} год", variant_number, year));
    println!("+---------+--------------------------------------------------------+");
    println!("| {:^64} |", "РЕЗУЛЬТАТЫ");
    println!("+------------+---+-------------------------------------------------+");

    for (verdict, task_number) in verdicts.iter().zip(1..) {
        println!("| Задание {:<2} | {} | {:<47} |",
                 task_number,
                 match *verdict {
                     Verdict::Correct => "+",
                     Verdict::PartiallyCorrect(_) => "±",
                     Verdict::Incorrect(_) => " ",
                 },
                 short_verdict_description(verdict));
    }

    println!("+------------+---+-------------------------------------------------+");
    println!("| ИТОГО      | {:<51} |",
             format!("решено (верно или частично верно) {} из {} заданий,",
                     verdicts.iter().filter(|x| !x.is_incorrect()).count(),
                     verdicts.len()));
    println!("|            | {:<51} |",
             format!("набрано {} баллов из {} — {}%",
                     sum,
                     max_sum,
                     percentage));
    println!("+------------+-----------------------------------------------------+");
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program_name = args[0].clone();

    let mut opts: Options = Options::new();
    opts.optflag("p",
                 "percentage-only",
                 "выводить только процент выполнения");

    let matches: getopts::Matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            let error_message = match f {
                Fail::ArgumentMissing(opt) => {
                    format!("Параметр “{}” требует аргумента.",
                            opt)
                }
                Fail::OptionDuplicated(opt) => {
                    format!("Параметр “{}” не может повторяться.",
                            opt)
                }
                Fail::OptionMissing(opt) => {
                    format!("Требуется параметр “{}”.", opt)
                }
                Fail::UnexpectedArgument(opt) => {
                    format!("Параметр “{}” не требует аргумента.",
                            opt)
                }
                Fail::UnrecognizedOption(opt) => {
                    format!("Неизвестный параметр: “{}”.", opt)
                }
            };
            println!("ОШИБКА. {}", error_message);
            print_help(&program_name);
            return;
        }
    };

    if matches.free.is_empty() {
        print_help(&program_name);
        return;
    }

    if matches.free.len() > 1 {
        println!("Укажите только одно имя файла.");
        print_help(&program_name);
        return;
    }

    let filename = matches.free[0].clone();
    let opened = OpenOptions::new().read(true).open(&filename);
    let file: File;
    let mut reader;

    if let Ok(f) = opened {
        file = f;
        reader = BufReader::new(file);
    } else {
        println!("Невозможно открыть файл “{}” для чтения ответов.",
                 filename);
        println!("Возможно, файл не существует или недостаточно прав для его чтения.");
        return;
    }

    let mut buffer = String::new();
    reader.read_line(&mut buffer).ok();

    let fields: Vec<&str> = buffer.trim().split(',').collect();
    if fields.len() != 6 {
        println!("Неверный формат файла. Первая строка файла должна содержать");
        println!("информацию о студенте, номер группы, год и номер варианта.");
        return;
    }
    let surname = fields[0];
    let given_names = fields[1];
    let patronymic = fields[2];
    let group = fields[3];
    let year = fields[4].parse();
    if year.is_err() {
        println!("Неверный формат файла. Год должен быть записан по григорианскому");
        println!("календарю в виде десятичного числа.");
        return;
    }
    let year: u32 = year.unwrap();

    let variant_number = fields[5].parse();
    if variant_number.is_err() {
        println!("Неверный формат файла. Номер варианта должен быть целым неотрицательным числом.");
        return;
    }
    let variant_number: u32 = variant_number.unwrap();

    let student: Student = Student::new(surname, given_names, patronymic, group);
    let generator = Generator::new(&student, year, variant_number);
    let tasks = generator.generate_task_suite();
    let mut answers = vec![];
    let mut verdicts = vec![];

    for task in &tasks {
        let mut buffer = String::new();
        reader.read_line(&mut buffer).ok();
        let line = buffer.trim();
        let answer: Answer;

        match Answer::from_csv(&line) {
            Ok(a) => answer = a,
            Err(_) => {
                println!("Ошибка при чтении ответа на задание № {} из файла.",
                         task.number());
                println!("Возможно, файл ответов “{}” повреждён.",
                         &filename);
                println!("Попробуйте сгенерировать файл ответов снова.");
                return;
            }
        }

        let result = task.check(&answer);

        answers.push(answer);
        verdicts.push(result);
    }

    // Count percentage.

    let mut sum = 0;
    let mut max_sum = 0;

    for (task, verdict) in tasks.iter().zip(&verdicts) {
        sum += verdict.points() * task.complexity();
        max_sum += Verdict::max_points() * task.complexity();
    }

    let percentage = sum * 100 / max_sum;

    // Print out the results.

    if matches.opt_present("p") {
        println!("{}", percentage);
        return;
    }

    // If no “--percentage-only” option is present, print out the whole report.

    print_header(&student,
                 variant_number,
                 year,
                 &verdicts,
                 sum,
                 max_sum,
                 percentage);

    let report = tasks.iter().zip(answers).zip(verdicts);

    for ((task, answer), verdict) in report {
        println!();

        let correctness_word = match verdict {
            Verdict::Correct => "верно",
            Verdict::PartiallyCorrect(_) => "частично верно",
            Verdict::Incorrect(_) => "неверно",
        };

        println!("{:~^68}", format!(" ЗАДАНИЕ {} ", task.number()));
        println!("{: ^68}",
                 format!(" решено {}, баллы: {} из {} ",
                         correctness_word,
                         verdict.points() * task.complexity(),
                         Verdict::max_points() * task.complexity()));

        println!();
        print_task(&task);

        println!();
        if verdict.is_correct() {
            println!("Принятый верный ответ:");
            print_answer(&answer);
        } else {
            println!("Принятый ответ:");
            print_answer(&answer);
            println!();
            println!("Правильный ответ:");
            print_answer(&task.correct_answer());
        }
    }
    println!();
}
