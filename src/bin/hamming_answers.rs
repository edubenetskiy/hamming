extern crate hamming;
extern crate regex;

use hamming::task::{Task, Answer, Verdict};
use hamming::student::Student;
use hamming::generator::Generator;

use regex::Regex;

use std::env;

pub const COPYING: &'static str = "
Copyright © 2016 Егор Дубенецкий
Лицензия MIT: https://opensource.org/licenses/MIT
Это свободное ПО: вы можете изменять и распространять его.
Нет НИКАКИХ ГАРАНТИЙ до степени, разрешённой законом.
";

fn print_task(task: &Task) {
    println!("Задание:");
    println!("    Принята двоичная последовательность: {}.",
             task.sequence());
    for line in task.sequence().as_table().lines() {
        println!("    {}", line);
    }
    println!("    Имеются ли в сообщении ошибки?");
}

fn print_answer(answer: &Answer) {
    match *answer {
        Answer::ReceivedCorrectly => println!("    Последовательность принята без ошибок."),
        Answer::ReceivedWithMistake { ref position, ref corrected_sequence } => {
            println!("    В последовательности была допущена ошибка в бите {}.",
                     position);
            println!("    Исправленная последовательность: {}.",
                     corrected_sequence);
            for line in corrected_sequence.as_table().lines() {
                println!("    {}", line);
            }
        }
    }
}

fn print_usage(program_name: &str) {
    println!("Использование: {} ФАМИЛИЯ ИМЯ ОТЧЕСТВО ГРУППА ГОД ВАРИАНТ",
             program_name);
    println!("Выводит ответы к заданиям по синтезу и анализу кода Хэмминга.");
    println!();
    println!("ФАМИЛИЯ, ИМЯ и ОТЧЕСТВО должны быть написаны кириллицей в соответствии");
    println!();
    println!("с данными ИСУ ИТМО в любом регистре. Если ОТЧЕСТВО отсутствует, необходимо");
    println!("поставить дефисоминус “-”, обычно имеющийся на клавиатуре.");
    println!("Если имя или фамилия состоит из нескольких слов, разделённых пробелами,");
    println!("не забудьте заключить их в одинарные или двойные кавычки.");
    println!("Номер ГРУППЫ должен быть записан в виде буквенно-цифрового кода, например");
    println!("“P3110”. Номер ВАРИАНТА должен быть записан арабскими цифрами согласно");
    println!("выданному преподавателем варианту, например “42”. ГОД должен соответствовать");
    println!("текущей дате по григорианскому календарю.");
    println!();
    println!("Примеры:");
    println!("    hamming_answers Абстрактный Павел Васильевич Z2170 2020 42");
    println!("    hamming_answers Клей 'Кассиус Марселлус' - N3201 2020 57");
}

fn print_help(program_name: &str) {
    print_usage(program_name);
    println!();
    println!("Версия {}.", hamming::version());
    println!("{}", COPYING);
}

fn print_header(student: &Student, variant_number: u32, year: u32) {
    println!("+------------------------------------------------------------------+");
    println!("| {:^64} |", "ИНФОРМАЦИЯ О СТУДЕНТЕ");
    println!("+---------+--------------------------------------------------------+");
    println!("| ФИО     | {:<54} |",
             format!("{} {} {}",
                     student.surname(),
                     student.given_names(),
                     student.patronymic()));
    println!("| Группа  | {:<54} |", student.group());
    println!("| Вариант | {:<54} |",
             format!("№ {}, {} год", variant_number, year));
    println!("+---------+--------------------------------------------------------+");
    println!("|  ВНИМАНИЕ!   Проверьте вышеприведённые данные и исправьте        |");
    println!("|              параметры командной строки, если это необходимо.    |");
    println!("+------------------------------------------------------------------+");
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let program_name = &args[0];

    if args.len() != 7 {
        println!("Ошибка синтаксиса.");
        print_help(&program_name);
        return;
    }

    let name_re: Regex = Regex::new(r"^[- '\w]+$").unwrap();
    let group_re: Regex = Regex::new(r"^[:alnum:]+$").unwrap();

    let surname = args[1].trim().to_uppercase();
    let given_names = args[2].trim().to_uppercase();
    let patronymic = args[3].trim().to_uppercase();

    if !name_re.is_match(surname.as_str()) {
        println!("Фамилия должна быть записана кириллицей.");
        return;
    }

    if !name_re.is_match(given_names.as_str()) {
        println!("Имя должно быть записано кириллицей.");
        return;
    }

    if !name_re.is_match(patronymic.as_str()) {
        println!("Отчество должно быть записано кириллицей.");
        println!("Если отчество отсутствует, укажите “-”.");
        return;
    }

    let group = args[4].trim().to_uppercase();
    if !group_re.is_match(group.as_str()) {
        println!("Номер группы должен быть записан в виде алфавитно-цифрового");
        println!("кода, где все буквы (если имеются) записаны латиницей.");
        return;
    }

    let year = args[5].trim().parse();
    if year.is_err() {
        println!("Год должен быть записан по григорианскому календарю в виде десятичного числа.");
        return;
    }
    let year: u32 = year.unwrap();

    let variant_number: Result<u32, _> = args[6].trim().parse();
    if variant_number.is_err() {
        println!("Номер варианта должен быть целым неотрицательным числом.");
        return;
    }
    let variant_number = variant_number.unwrap();

    let student: Student = Student::new(surname, given_names, patronymic, group);
    let generator = Generator::new(&student, year, variant_number);
    let tasks = generator.generate_task_suite();

    print_header(&student, variant_number, year);
    println!();

    for task in tasks {
        println!("{:~^68}", format!(" ЗАДАНИЕ {} ", task.number()));
        println!("{: ^68}",
                 format!("максимум {} баллов",
                         Verdict::max_points() * task.complexity()));
        println!();
        print_task(&task);
        println!();
        println!("Ответ:");
        print_answer(&task.correct_answer());
        println!();
    }
}
